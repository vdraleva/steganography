#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <stdbool.h>
#include "enter_directory.h"

void enter_directory() {
    char dir_name[100];
    printf("Enter directory name: ");
    scanf("%s", dir_name);

    DIR *dir = opendir(dir_name);

    if (dir == NULL) {
        printf("Error: Cannot open directory.\n");
        exit(1);
    }

    printf("Directory contents:\n");

    struct dirent *entry;

    while ((entry = readdir(dir)) != NULL) {
        printf("%s\n", entry->d_name);
    }

    closedir(dir);
}
