#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <stdbool.h>
#include "encode.h"

void encode(char *message, char *file_in, char *file_out) {
    // Open input file
    FILE *input_file = fopen(file_in, "rb");
    if (input_file == NULL) {
        fprintf(stderr, "Error: Could not open input file '%s'\n", file_in);
        exit(1);
    }

    // Read BMP header
    BMPHeader header;
    fread(&header, sizeof(header), 1, input_file);
    if (header.type != 0x4D42) {
        fprintf(stderr, "Error: File '%s' is not a BMP file\n", file_in);
        exit(1);
    }

    // Read DIB header to get image width and height
    DIBHeader dib_header;
    fread(&dib_header, sizeof(dib_header), 1, input_file);
    int32_t width = dib_header.width;
    int32_t height = dib_header.height;

    // Calculate image data size
    uint32_t data_size = dib_header.imageSize;
    if (data_size == 0) {
        data_size = width * height * (dib_header.bitsPerPixel / 8);
    }
    printf("Data size: %u bytes\n", data_size);

    // Allocate memory for image data
    uint8_t *pixels = malloc(data_size);
    if (pixels == NULL) {
        fprintf(stderr, "Error: Could not allocate memory for image data\n");
        exit(1);
    }

    // Read image data
    fread(pixels, 1, data_size, input_file);

    // Close input file
    fclose(input_file);

    //Call encode method
    encode_message(pixels,data_size,message);

    // Open output file
    FILE *output_file = fopen(file_out, "wb");
    if (output_file == NULL) {
        fprintf(stderr, "Error: Could not open output file '%s'\n", file_out);
        exit(1);
    }

    // Write BMP header and DIB header to output file
    fwrite(&header, sizeof(header), 1, output_file);
    fwrite(&dib_header, sizeof(dib_header), 1, output_file);

    // Write image data to output file
    fwrite(pixels, 1, data_size, output_file);


    // Close output file
    fclose(output_file);

    // Free memory
    free(pixels);
}
void encode_message(uint8_t *pixels, size_t data_size, const char *message) {
    // Calculate length of message and message in binary
    size_t msg_len = strlen(message);
    size_t msg_binary_len = (msg_len + 1) * 8; // Add 4 bytes for message length
    uint8_t *msg_binary = malloc(msg_binary_len);
    if (msg_binary == NULL) {
        fprintf(stderr, "Error: Could not allocate memory for binary message\n");
        exit(1);
    }
    memset(msg_binary, 0, msg_binary_len); // Initialize binary message with zeros

    // Encode message length
    *(uint32_t *)msg_binary = (uint32_t)msg_len;

    // Convert message to binary
    for (size_t i = 0; i < msg_len; i++) {
        for (size_t j = 0; j < 8; j++) {
            msg_binary[(i * 8) + j] = (message[i] >> j) & 1;
        }
    }

    // Encode message in pixels
    size_t msg_index = 0;
    for (size_t i = 32; i < data_size; i += 3) {
        if (msg_index < msg_binary_len) {
            pixels[i] = (pixels[i] & 0xFE) | msg_binary[msg_index++];
        }
        if (msg_index < msg_binary_len) {
            pixels[i + 1] = (pixels[i + 1] & 0xFE) | msg_binary[msg_index++];
        }
        if (msg_index < msg_binary_len) {
            pixels[i + 2] = (pixels[i + 2] & 0xFE) | msg_binary[msg_index++];
        }
        else {
            break;
        }
    }

    free(msg_binary);
}
