#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <stdbool.h>
#include "decode.h"
#include "encode.h"

#define MAX_MSG_LEN 1000
#define BYTE_SIZE 8

void decode(char* file_in, char* file_out) {
    // Open input file
    FILE *input_file = fopen(file_in, "rb");
    if (input_file == NULL) {
        fprintf(stderr, "Error: Could not open input file '%s'\n", file_in);
        exit(1);
    }

    // Read BMP header
    BMPHeader header;
    fread(&header, sizeof(header), 1, input_file);
    if (header.type != 0x4D42) {
        fprintf(stderr, "Error: File '%s' is not a BMP file\n", file_in);
        exit(1);
    }

    // Read DIB header to get image width and height
    DIBHeader dib_header;
    fread(&dib_header, sizeof(dib_header), 1, input_file);
    int32_t width = dib_header.width;
    int32_t height = dib_header.height;

    // Calculate image data size
    uint32_t data_size = dib_header.imageSize;
    if (data_size == 0) {
        data_size = width * height * (dib_header.bitsPerPixel / 8);
    }

    // Allocate memory for image data
    uint8_t *pixels = malloc(data_size);
    if (pixels == NULL) {
        fprintf(stderr, "Error: Could not allocate memory for image data\n");
        exit(1);
    }

    // Read image data
    fread(pixels, 1, data_size, input_file);

    // Close input file
    fclose(input_file);

    decode_local(pixels, data_size, file_out);
}

void decode_local(uint8_t* pixels, size_t data_size, char* file_out) {
    size_t msg_binary_len = 0;

    // Read message length from pixels
    for (size_t i = 0; i < 32; i += 8) {
        msg_binary_len |= (pixels[i] & 1) << i;
    }

    // Allocate memory for binary message
    uint8_t *msg_binary = malloc(msg_binary_len);
    if (msg_binary == NULL) {
        fprintf(stderr, "Error: Could not allocate memory for binary message\n");
        exit(1);
    }

    // Convert pixels to binary message
    size_t msg_index = 0;
    for (size_t i = 32; i < data_size; i += 3) {
        if (msg_index < msg_binary_len) {
            msg_binary[msg_index++] = pixels[i] & 1;
        }
        if (msg_index < msg_binary_len) {
            msg_binary[msg_index++] = pixels[i + 1] & 1;
        }
        if (msg_index < msg_binary_len) {
            msg_binary[msg_index++] = pixels[i + 2] & 1;
        }
        else {
            break;
        }
    }

    // Convert binary message to ASCII
    size_t msg_len = msg_binary_len / 8;
    char *message = malloc(msg_len + 1);
    if (message == NULL) {
        fprintf(stderr, "Error: Could not allocate memory for message\n");
        exit(1);
    }
    for (size_t i = 0; i < msg_len; i++) {
        message[i] = 0;
        for (size_t j = 0; j < 8; j++) {
            message[i] |= msg_binary[i * 8 + j] << j;
        }
    }
    message[msg_len] = '\0';

    // Print the decoded message
    //printf("Decoded message: %s\n", message);

    // Open output file
    FILE *output_file = fopen(file_out, "wb");
    if (output_file == NULL) {
        printf("\nError opening output file\n");
        fclose(output_file);
        return;
    }
    fprintf(output_file, "%s", message);

    // Close files
    fclose(output_file);
    free(msg_binary);
    free(message);


    printf("\nMessage decoded successfully!\n");
}

