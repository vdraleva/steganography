#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <stdbool.h>
#include "encode.h"
#include "decode.h"
#include "enter_directory.h"

#define MAX_MSG_LEN 1000
#define BYTE_SIZE 8

int main() {
    char choice;
    char message[MAX_MSG_LEN];
    char input_filename[MAX_MSG_LEN];
    char output_filename[MAX_MSG_LEN];

    printf("Welcome to the Viki's steganography program.\n");
    while (1) {
        printf("Please choose one of the following options:\n");
        printf("-o Enter directory where to find the files.\n");
        printf("-i Enter input image file.\n");
        printf("-e Encode a message.\n");
        printf("-d Decode a message.\n");
        printf("-q Quit the program.\n");
        printf("Enter command: ");
        scanf(" %c", &choice);

        switch (choice) {
            case 'o':
                enter_directory();
                break;
            case 'i':
                printf("Enter the input image file name: ");
                scanf("%s", input_filename);
                break;
            case 'e':
                printf("Enter the message to encode: ");
                scanf(" %[^\n]s", message);

                if (strlen(input_filename) == 0) {
                    printf("Enter the input image file name: ");
                    scanf("%s", input_filename);
                }

                printf("Enter the output image file name: ");
                scanf("%s", output_filename);

                encode(message, input_filename, output_filename);
                //printf("Message encoded successfully.\n");
                break;
            case 'd':
                printf("Enter the input image file name: ");
                scanf("%s", input_filename);

                printf("Enter the output text file name: ");
                scanf("%s", output_filename);

                decode(input_filename, output_filename);
                printf("Message decoded successfully.\n");
                break;
            case 'q':
                printf("Exiting the program.\n");
                return 0;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }
}
